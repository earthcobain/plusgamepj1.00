package buu.informatics.s59160106.plusproject1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import buu.informatics.s59160106.plusproject1.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this ,R.layout.activity_main)


        val txtnumber1 = binding.txtNum1
        val txtnumber2 = binding.txtNum2
        val btnNumber1 = binding.btnNumber1
        val btnNumber2 = binding.btnNumber2
        val btnNumber3 = binding.btnNumber3
        val txtResult = binding.txtResult
        val txtWin = binding.txtWin
        val txtLose = binding.txtLose

        round(txtnumber1,txtnumber2,btnNumber1,btnNumber2,btnNumber3)

        binding.btnNumber1.setOnClickListener {
            if(checkTrue(txtnumber1,txtnumber2,btnNumber1)){
                txtResult.text = "ถูกต้อง"
                Win(txtWin)
            }else{
                txtResult.text = "ไม่ถูกต้อง"
                Lose(txtLose)
            }
            round(txtnumber1,txtnumber2,btnNumber1,btnNumber2,btnNumber3)
        }
        binding.btnNumber2.setOnClickListener {
            if(checkTrue(txtnumber1,txtnumber2,btnNumber2)){
                txtResult.text = "ถูกต้อง"
                Win(txtWin)
            }else{
                txtResult.text = "ไม่ถูกต้อง"
                Lose(txtLose)
            }
            round(txtnumber1,txtnumber2,btnNumber1,btnNumber2,btnNumber3)
        }
        binding.btnNumber3.setOnClickListener {
            if(checkTrue(txtnumber1,txtnumber2,btnNumber3)){
                txtResult.text = "ถูกต้อง"
                Win(txtWin)
            }else{
                txtResult.text = "ไม่ถูกต้อง"
                Lose(txtLose)
            }
            round(txtnumber1,txtnumber2,btnNumber1,btnNumber2,btnNumber3)
        }
    }

    fun round(txtnumber1:TextView,txtnumber2:TextView,btnNumber1:Button,btnNumber2:Button,btnNumber3:Button) {
        txtnumber1.text  =randomNumber().toString()
        txtnumber2.text  =randomNumber().toString()
        val result  = Integer.parseInt(txtnumber1.text.toString()) + Integer.parseInt(txtnumber2.text.toString())
        when (randomButton()) {
            0 -> {
                btnNumber1.text = result.toString()
                btnNumber2.text = (result+1).toString()
                btnNumber3.text = (result+2).toString()
            }
            1 -> {
                btnNumber1.text = (result-1).toString()
                btnNumber2.text = result.toString()
                btnNumber3.text = (result+1).toString()
            }
            2 -> {
                btnNumber1.text = (result-2).toString()
                btnNumber2.text = (result-1).toString()
                btnNumber3.text = result.toString()
            }
        }
    }

    fun checkTrue(txtnumber1: TextView,txtnumber2 : TextView,btnNumber: Button): Boolean {
        val result  = Integer.parseInt(txtnumber1.text.toString()) + Integer.parseInt(txtnumber2.text.toString())
        if(result == Integer.parseInt(btnNumber.text.toString())){
            return true
        }
        return false
    }
    fun randomNumber(): Int {
        return (1..10).random()
    }
    fun randomButton(): Int {
        return (0..2).random()
    }
    fun Win(txtWin: TextView) {
        txtWin.text = (Integer.parseInt(txtWin.text.toString()) +1).toString()
    }
    fun Lose(txtLose: TextView) {
        txtLose.text = (Integer.parseInt(txtLose.text.toString()) +1).toString()
    }
}